const express = require('express');
const app = express();
const fs = require('fs');
const path = require('path');
const morgan = require('morgan');

app.use(express.json());
app.use(morgan('tiny'));

const regExp = new RegExp(/^[A-z1-9.]+\.(txt|log|json|xml|js|yaml)$/);

app.post('/api/files', (req, res) => {
    let body = req.body;
    try {
       if (body.content === undefined) {
            res.status(400).json({massage: "Please specify 'content' parameter"});
        } else if (!regExp.test(body.filename)) {
            res.status(400).json({massage: "The file supports log, txt, json, yaml, xml, js extensions"});
        } else {
            fs.writeFile(path.join('./api/files', body.filename), body.content, (req, res, err) => {
            })
            res.status(200).json( {massage: "File created successfully"});
        }
    } catch (err) {
        res.status(500).json({massage: "Server error: " +err});
        console.error(err);
    }


})


app.get('/getFiles', (req, res) => {
    let filesInDir = [];
    fs.readdir('./api/files', function (err, files) {
        if (err) {
            res.status(400).send(err);
        }
        files.forEach(file => filesInDir.push(file));
        res.status(200).json(filesInDir);
    });
})

app.get('/api/files/:filename', (req, res) => {
    const param = req.params;
    const messages = {
        success: {
            "message": "success",
            "filename": param.filename
        },
        error: `No file with ${param.filename} filename found`
    }

    fs.readFile(path.join('./api/files', param.filename), 'utf-8', (err, data) => {
        if (err) {
            res.status(400).send(messages.error);
        } else {
            fs.readFile(path.join('./api/files', param.filename), 'utf-8', (err, data) => {
                res.status(200).json(messages.success)
            })
        }
    })
})

app.listen(8080);

